import React from 'react';
import './NotFound.css';
import Header from '../../common/Header/Header.jsx';
import Footer from '../../common/Footer/Footer.jsx';
import Oops from './Oops/Oops.jsx';

function NotFound() {
    return (
        <>
        <div class="wrapper">
        <Header />
        <div className="notfound-container">
            <Oops />
        </div>
        <Footer />
        </div>
        </>  
    )
}

export default NotFound;