import React from 'react';
import './Oops.css';
import {ReactComponent as Image} from '../../../assets/pigDoubtFull.svg';
import Botao from '../../../common/Botao/Botao';

function Oops() {
    return (
        <div className="error-container">
            <h1>Erro 404</h1>
            <Image className="error-img"/>
            <p>Ops! Parece que o endereço que você digitou está incorreto =(</p>
            <Botao label="voltar" link="/"/>
        </div>
    )
}

export default Oops;
