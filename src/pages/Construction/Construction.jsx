import React from 'react';
import './Construction.css';
import Header from '../../common/Header/Header.jsx';
import Footer from '../../common/Footer/Footer.jsx';
import Building from './Building/Building.jsx';

function Construction() {
    return (
        <>
        <div className="wrapper">
            <Header />
            <div className="construction-container">
                <Building />
            </div>
            <Footer />
        </div>
        </>
    )
}

export default Construction;