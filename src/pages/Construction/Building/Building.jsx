import React from 'react';
import './Building.css';
import {ReactComponent as Image} from '../../../assets/construction.svg'
import Botao from '../../../common/Botao/Botao';

function Building() {
    return (
        <div className="building-container">
            <h1>Em construção</h1>
            <Image />
            <p>Parece que essa página ainda não foi implementada =( <br/>
            Tente novamente mais tarde!</p>
            <Botao label="voltar" link="/"/>
        </div>
    )
}

export default Building;
