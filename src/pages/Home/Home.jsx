import React from 'react';
import Header from '../../common/Header/Header.jsx';
import Footer from '../../common/Footer/Footer.jsx';
import About from './About/About.jsx';
import HowTo from './HowTo/HowTo.jsx';
import More from './More/More.jsx';
import './Home.css';

function Home() {
    return (
        <>
        <Header />
        <About />
        <HowTo />
        <More />
        <Footer />
        </>   
    )
}

export default Home;
