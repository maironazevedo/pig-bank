import React from 'react';
import Botao from '../../../common/Botao/Botao.jsx';
import {ReactComponent as Image} from '../../../assets/Pig2.svg'
import './More.css'

function More() {
    return (
        <div className="more-container">
            <div className="upper-info">
                <div className="text-area">
                    <h2>Já tem uma meta?</h2>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. 
                        Consequuntur, dicta veniam sit id tempora cupiditate sint 
                        vitae at? Voluptas eligendi saepe laudantium suscipit autem 
                        repellat velit porro quod reiciendis fugit?
                    </p>
                    <Botao label="ver metas" link="construction"/>
                </div>  
                <Image />
            </div>
            <div className="lower-info">
                <h2>Não tem? Comece agora mesmo!</h2>
                <Botao label="nova meta" link="construction"/>
            </div>
        </div>
    )
}

export default More;
