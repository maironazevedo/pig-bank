import React from 'react';
import Botao from '../../../common/Botao/Botao';
import {ReactComponent as Image} from '../../../assets/Pig1.svg';
import './About.css';


function About() {
    return (
        <div className="about-container">
            <div className="text-area">
                <h1>Pig Bank</h1>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt cupiditate soluta aliquid, nemo voluptatum quidem explicabo delectus corrupti neque accusamus, asperiores ullam vitae consectetur est ut amet atque eligendi magnam?</p>
                <Botao label="nova meta" link="construction"/>
            </div>
            <Image className="about-img"/>
        </div>
    )
}

export default About;
