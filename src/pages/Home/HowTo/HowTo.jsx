import React from 'react';
import {ReactComponent as ImageMeta} from '../../../assets/howToUse.svg';
import {ReactComponent as ImageProgress} from '../../../assets/progress.svg';
import {ReactComponent as ImageObjective} from '../../../assets/objective.svg';
import './HowTo.css';

function HowTo() {
    return (
        <div className="background">
            <div className="howto-container">
                <h2>Como usar</h2>
                <div className="card-field">
                    <div className="card">
                        <ImageMeta className="card-image"/>
                        <h3>DEFINA UMA META</h3>
                        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit.</p>
                    </div>
                    <div className="card">
                        <ImageProgress className="card-image"/>
                        <h3>ACOMPANHE O PROGRESSO</h3>
                        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit.</p>
                    </div>
                    <div className="card">
                        <ImageObjective className="card-image"/>
                        <h3>ALCANCE SEU OBJETIVO</h3>
                        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit.</p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default HowTo;
