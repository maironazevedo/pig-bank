import React, {useState, useEffect} from 'react';
import './Cotation.css';
import getCoin from '../../service/Api.js';
import MoneyCard from './MoneyCard/MoneyCard.jsx';
import Header from '../../common/Header/Header.jsx';
import Footer from '../../common/Footer/Footer.jsx';
import {ReactComponent as Image} from '../../assets/pigDoubtSliced.svg'

function Cotation() {
    const [coin, setCoin] = useState([]);

    useEffect(() => {
        getCoin(setCoin); 
    }, []);

    return (
        <>
        <Header />
        <h1>Cotação das moedas</h1>
        <main className="cotation-main">
            <div className="money-card-field">
                { coin.map(money => {
                    return (
                        <MoneyCard infos={money}/>
                        )
                    })
                }
            </div>
        </main>
        <footer className="page-footer">
            <div className="gradient"></div>
            <figure>
                <Image className="img"/>
            </figure>
            <Footer />      
        </footer>
        </>    
    )
}

export default Cotation;
