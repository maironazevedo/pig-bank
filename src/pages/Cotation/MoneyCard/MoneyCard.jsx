import React from 'react';
import './MoneyCard.css';

function getDateTime(infos) {
    // formato da string: "2020-01-23 00:46"
    let dateTime = "";
    let timeStamp = infos.create_date.split(" ");
    let currentDayList = timeStamp[0].split("-");
    let currentDay = currentDayList[2] + "/" + currentDayList[1] + "/" + currentDayList[0];
    let currentHour = timeStamp[1];

    dateTime = "Atualizado em " + currentDay + " às " + currentHour;    

    return dateTime;
}

function MoneyCard({infos}) {
    return (
        <div className="money-card">
            <div className="money-code">
                <p>{infos.name}</p>
                <h3>{infos.code}</h3>
            </div>
            <div className="money-info">
                <p>Máxima: {infos.high}</p>
                <p>Mínima: {infos.low}</p>
                <p>{getDateTime(infos)}</p>
            </div>
        </div>
    )
}

export default MoneyCard;
