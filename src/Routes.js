import React from 'react';

// Importando páginas
import Construction from './pages/Construction/Construction';
import Cotation from './pages/Cotation/Cotation';
import Home from './pages/Home/Home';
import NotFound from './pages/NotFound/NotFound';

// Importando dependencias de rotas 
import { 
    BrowserRouter as Router, 
    Switch, 
    Route, 
    Redirect
} from 'react-router-dom';


function Routes() {
    return (
        <Router>
            <Switch>
                { /* Definindo rotas */}
                <Route exact path="/home">
                    <Redirect to="/"/>
                </Route>
                <Route exact path="/">
                    <Home/>
                </Route>
                <Route exact path="/cotation">
                    <Cotation/>
                </Route>
                <Route exact path="/construction">
                    <Construction/>
                </Route>
                { /* Construindo Redirect */}
                <Route exact path="/404">
                    <NotFound/>
                </Route>
                <Route path="/">
                    <Redirect to="/404"/>
                </Route>
            </Switch>
        </Router>
    )
}

export default Routes;
