import React from 'react';
import './Botao.css';
import {Link} from 'react-router-dom'; 

function Botao(props) {
    return (
        <Link to={props.link}>
            <button className="botao">{props.label.toUpperCase()}</button>
        </Link>

    )

}

export default Botao;
