import React from 'react';
import Botao from '../../Botao/Botao.jsx';
import './Menu.css';

// muda o display do menu hamburguer
function changeFunction(div){ 
    div.classList.toggle("change"); 
}

// esconde os botões do modal
function hideButtons(div) {
    div.nextSibling.classList.toggle("do-not-show");
}

// função chamada no onclick, contendo as duas funções que serão executadas
function onClickFunctions(div) {
    changeFunction(div);
    hideButtons(div);
}

function Menu() {
    return (
        <>
        <div className="menu">
            <div className="container" onClick={(e) => {onClickFunctions(e.target)}}>
                <div className="bar1"></div>
                <div className="bar2"></div>
                <div className="bar3"></div>
            </div>
            <nav className="do-not-show">
                <Botao label={"início"} link="/"/>
                <Botao label={"cotação das moedas"} link="cotation"/>
            </nav>
        </div>
        </>
    )
}

export default Menu;
