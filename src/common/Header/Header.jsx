import React from 'react';
import Menu from './Menu/Menu.jsx';
import './Header.css';

function Header() {
    return (
        <header>
            <Menu />
        </header>
    )
}

export default Header;
